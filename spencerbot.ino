#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <ESP8266WebServer.h>

enum ConnectionMode {
  DISCONNECTED,
  AP,
  WIFI
};

enum ConnectionStatus {
  STARTING_UP,
  AP_NO_STATIONS,
  AP_STATION_CONNECTED,
  WIFI_READY
};

String ssid;
String password;
String apName = "SpencerBot";
enum ConnectionMode connectMode = DISCONNECTED;
enum ConnectionStatus connectionStatus = STARTING_UP;
ESP8266WebServer server(80);

unsigned long previousTime = 0;
bool blinkState = false;

const int wifiLedPin = 16;
const int apLedPin = 5;

/*
 * Manage Connectivity
 */
void setup() {
  Serial.begin(115200);
  EEPROM.begin(512);
  delay(10);
  pinMode(wifiLedPin, OUTPUT);
  digitalWrite(wifiLedPin, LOW);   
  pinMode(apLedPin, OUTPUT);
  digitalWrite(apLedPin, LOW);
  
  eepromReadWifiSettings();
  
  if(ssid.length() == 0 || password.length() == 0) {
    setConnectionMode(AP);
  } else {
    setConnectionMode(WIFI);
  }

  server.on("/aps", apListHandler);
  server.on("/wifi", HTTP_GET, wifiSettingsHandler);
}

void setConnectionMode(ConnectionMode mode) {
  connectMode = mode;
  switch(connectMode) {
    case DISCONNECTED:
      WiFi.softAPdisconnect(true);
      WiFi.disconnect();
      server.stop(); 
      break;
    case AP:
      WiFi.disconnect();
        WiFi.softAP(apName);
        Serial.println("Running Access Point 'RoverBot'");
        server.begin();
      break;
    case WIFI:
      WiFi.softAPdisconnect(true);
      if(connectToWifi()){
        server.begin();
        setConnectionStatus(WIFI_READY);
      } else {
        setConnectionMode(AP);
      }
  }
}

void setConnectionStatus(ConnectionStatus status) {
  switch(status) {
    case WIFI_READY:
      digitalWrite(wifiLedPin, HIGH);
      break;
  }
}

bool connectToWifi() {
  WiFi.begin(ssid, password);
  int secs = 0;
  bool blinkState = false;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    blinkState = !blinkState;
    digitalWrite(wifiLedPin, blinkState);
    if(secs >= 15 * 2) {
      Serial.println("Timeout exceeded waiting to connect");
      return false;
    }
    secs += 1;
  }
  
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  return true;
}

/*
 * Handlers
 */
void apListHandler() {
  String ssidList = "";
  int n = WiFi.scanNetworks();
  if (n == 0) {
    Serial.println("no networks found");
  } else {
    for (int i = 0; i < n; ++i) {
      ssidList += WiFi.SSID(i) + "\n";
    }
  }
  server.send(200,"text/html", ssidList);
}

void wifiSettingsHandler() {
  if ((server.arg("ssid") == "") || (server.arg("pw") == "")) {  
    server.send(400,"text/html", "ssid or pw not supplied");
  } else {
    ssid = server.arg("ssid");
    password = server.arg("pw");
    server.send(200,"text/html", "received");
    Serial.println(ssid);
    Serial.println(password);
    setConnectionMode(WIFI);
    eepromWriteWifiSettings();
  }
}

/*
 * EEPROM utilities
 */

void eepromReadWifiSettings() {
  Serial.println("attempting to retrieve stored wifi settings");
  ssid = readString(0, 32);
  password = readString(32, 100);
  Serial.println("ssid: " + ssid);
  Serial.println("password: " + password);
}


void eepromWriteWifiSettings() {
  Serial.println("attempting to store wifi settings");
  writeString(0, ssid);
  writeString(32, password);
}

String readString(char add, int sz)
{
  int i;
  char data[sz]; //Max 100 Bytes
  int len=0;
  unsigned char k;
  k=EEPROM.read(add);
  while(k != '\0' && len<=sz)   //Read until null character
  {    
    k=EEPROM.read(add+len);
    data[len]=k;
    len++;
  }
  data[len]='\0';
  return String(data);
}

void writeString(char add,String data)
{
  int _size = data.length();
  int i;
  for(i=0;i<_size;i++)
  {
    EEPROM.write(add+i,data[i]);
  }
  EEPROM.write(add+_size,'\0');
  EEPROM.commit();
}


/*
 * Loop
 */

void loop() {
  if(connectMode != DISCONNECTED) {
    server.handleClient();
  }
  // check status every n seconds
  // if mode == wifi but WiFi.status() != WL_CONNECTED
  //  you've been disconnected, handle this
}  
